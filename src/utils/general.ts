import { Request, Response } from 'express';
import { Model } from 'objection';

import CrudOptions, { RouteName } from '../crud-options';

export function hasOptions(opts: any): opts is CrudOptions {
    if (!opts) {
        return false;
    }

    if (typeof opts !== 'object' && typeof opts !== 'function') {
        return false;
    }

    return opts;
}

export function hasRoute(opts: CrudOptions, routeName: RouteName) {
    if (!hasOptions(opts)) {
        return true;
    }

    const { routes } = opts;

    if (!routes || typeof routes !== 'object') {
        return true;
    }

    if (Array.isArray(routes)) {
        return routes.includes(routeName);
    }

    const route = routes[routeName];
    return route === void 0 || !!route;
}

export function wrapResult<T extends Model, E extends { routeName: RouteName }>(opts: CrudOptions<T>,
    req: Request, res: Response, data: any, extra: E) {

    if (typeof opts?.resultWrap === 'function') {
        return opts.resultWrap(data, req, extra);
    } else {
        return data;
    }
}
