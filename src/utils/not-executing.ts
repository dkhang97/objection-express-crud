import { NextFunction, Request, Response } from 'express';
import { Model } from 'objection';

import CrudOptions, { RouteName } from '../crud-options';

export async function notExecuting<T extends Model>(opts: CrudOptions<T>,
    req: Request, res: Response, next: NextFunction,
    optField: keyof CrudOptions, routeName: RouteName, data?: T) {

    if (typeof opts?.[optField] === 'function') {
        const fn: any = opts[optField];
        const hasData = data === void 0;
        const call = hasData ? fn(req) : fn(req, data);

        if (!await Promise.resolve(call)) {
            if (typeof opts?.handleForbidden === 'function') {
                if (hasData) {
                    opts.handleForbidden(req, res, next, routeName, data);
                } else {
                    opts.handleForbidden(req, res, next, routeName);
                }
            } else {
                res.sendStatus(403);
            }
            return true;
        }
    }

    return false;
}
