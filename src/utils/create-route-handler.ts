import { NextFunction, Request, RequestHandler, Response } from 'express';
import { Model } from 'objection';

import CrudOptions, { RouteName } from '../crud-options';
import { wrapResult } from './general';
import { notExecuting } from './not-executing';

export default function createRouteHandler<T extends typeof Model, D, DE extends { data: any, extra?: any }>(
    model: T, opts: CrudOptions<T['prototype']>,
    optCheckField: keyof CrudOptions, routeName: RouteName,
    executor: (req: Request, res: Response, model: T, data?: D) => DE | Promise<DE>,
    preDataFn?: (req: Request, model: T) => Promise<D> | D): RequestHandler {

    return async (req: Request, res: Response, next: NextFunction) => {
        try {
            const hasPreData = typeof preDataFn === 'function';
            const preData = !hasPreData ? undefined
                : await Promise.resolve(preDataFn(req, model));

            if (await notExecuting(opts, req, res, next, optCheckField, routeName, preData as any)) {
                return;
            }

            let promiseResult: any;

            if (hasPreData) {
                if (preData) {
                    promiseResult = await Promise.resolve(executor(req, res, model, preData));
                } else {
                    if (typeof opts?.handleNotFound === 'function') {
                        opts.handleNotFound(req, res, next, routeName);
                    } else {
                        res.sendStatus(404);
                    }
                    return;
                }
            } else {
                promiseResult = await Promise.resolve(executor(req, res, model));
            }

            const { data, extra } = promiseResult || {};

            if (routeName !== 'list' && !data) {
                if (typeof opts?.handleNotFound === 'function') {
                    opts.handleNotFound(req, res, next, routeName);
                } else {
                    res.sendStatus(404);
                }
                return;
            }

            const responseData = await Promise
                .resolve(wrapResult(opts, req, res, data, Object.assign(extra || {}, { routeName })));

            if (typeof opts?.handleResponse === 'function') {
                opts.handleResponse(req, res, next, routeName, responseData);
            } else {
                res.json(responseData);
            }
        } catch (e) {
            if (typeof opts?.handleError === 'function') {
                opts.handleError(req, res, next, routeName, e);
            } else {
                next(e);
            }
        }
    };
}
