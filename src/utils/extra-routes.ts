import { RequestHandler } from 'express';

import CrudOptions, { ExtraRoutes, RouteName } from '../crud-options';
import { hasOptions } from './general';

function extraRoutes(opts: CrudOptions, routeName: RouteName, position: 'start' | 'end'): RequestHandler[] {
    if (!hasOptions(opts)) {
        return [];
    }

    let routes: ExtraRoutes;
    if (position === 'start') {
        routes = opts.preRoutes;
    } else if (position === 'end') {
        routes = opts.postRoutes;
    } else {
        return [];
    }

    if (Array.isArray(routes)) {
        return routes;
    } else if (typeof routes === 'function') {
        return [routes];
    } else if (routes && typeof routes === 'object') {
        const innerRoutes = routes[routeName];

        if (Array.isArray(innerRoutes)) {
            return innerRoutes;
        } else if (typeof innerRoutes === 'function') {
            return [innerRoutes];
        }
    }


    return [];
}

export function combineExtraRoutes(opts: CrudOptions, routeName: RouteName, handler: RequestHandler) {
    return [
        ...extraRoutes(opts, routeName, 'start'),
        handler,
        ...extraRoutes(opts, routeName, 'end'),
    ];
}