import { NextFunction, Request, RequestHandler, Response } from 'express';
import {
    AnyQueryBuilder,
    GraphOptions,
    InsertGraphOptions,
    Model,
    QueryBuilder,
    RelationExpression,
    SingleQueryBuilder,
} from 'objection';

export type RouteName = "list" | "detail" | "insert" | "updatePatch" | "updatePut" | "delete";
export type ExtraRoutes = Partial<Record<RouteName, RequestHandler | RequestHandler[]>>
    | RequestHandler | RequestHandler[];

export interface ResultWrapExtras {
    routeName: RouteName, offset?: number;
    actualPage?: number, actualSize?: number;
    query?: AnyQueryBuilder;
    getCount?(): Promise<number>;
    getTotalPage?(): Promise<number>;
    hasMore?(): Promise<boolean>;
}

export interface CrudCreateQueryOpts {
    graphJoined?: boolean;
    mainQuery?: boolean;
}

export type CrudCreateQuery<T extends Model> = (opts?: CrudCreateQueryOpts) => CrudQueryBuilder<T>

type CountFn<T extends Model> = (crudParam: {
    createQuery: CrudCreateQuery<T>,
    wrapCall: (query: AnyQueryBuilder) => Promise<number>,
    createCountQuery(): CrudQueryBuilder<T>;
}, req: Request) => number | Promise<number>;

type ListRangeFn<T extends Model> = (crudParam: {
    createQuery: CrudCreateQuery<T>,
    queryType: 'main' | 'hasMore',
}, limit: number, offset: number) => AnyQueryBuilder;

export interface CrudQueryBuilder<T extends Model> extends QueryBuilder<T> {
    withCrudGraph(expr: RelationExpression<T>, options?: GraphOptions): QueryBuilder<T>;
    asCrudGraphFetched(): QueryBuilder<T>;
    asCrudGraphJoined(): QueryBuilder<T>;
    readonly isCrudGraphJoined: boolean;
    readonly isCrudMainQuery: boolean;
}

export default interface CrudOptions<T extends Model = Model> {
    insertGraphOptions?: InsertGraphOptions;
    /**
     * Register express routing for objection, default option is **true**
     */
    routes?: Partial<Record<RouteName, boolean>> | RouteName[];
    /**
     * Register handler(s) before objection handler
     */
    preRoutes?: ExtraRoutes;
    /**
     * Register handler(s) after objection handler
     */
    postRoutes?: ExtraRoutes;
    defaultPageSize?: number;
    /**
     * Generate limit & offset select for `listFn`
     * `true` to use `Model.idColumn`, any string to select distinct column or function to create range query
     */
    listRangeFn?: true | string | string[] | ListRangeFn<T>;
    /**
     * Specify count column or generate count query for `listFn`
     */
    countFn?: string | string[] | CountFn<T>;
    /**
     * Function to modify query builder for listing route
     * @param query QueryBuilder to construct
     * @param req Request Info
     */
    listFn?<Q extends AnyQueryBuilder>(query: CrudQueryBuilder<T>, req: Request): Q | void;
    /**
     * Function to modify query builder for detail route
     * @param query QueryBuilder to construct
     * @param req Request Info
     */
    detailFn?(query: SingleQueryBuilder<QueryBuilder<T>>, req: Request): void | Promise<void>;
    /**
     * Wrap function of query result
     * @param result Original Result
     * @param req Request Info
     * @param extra Extra info from query
     */
    resultWrap?<R = any>(result: any, req: Request, extra: ResultWrapExtras): R | Promise<R>;
    handleResponse?(req: Request, res: Response, next: NextFunction, routeName: RouteName, data: any): void;
    handleForbidden?(req: Request, res: Response, next: NextFunction, routeName: RouteName, data?: any): void;
    handleNotFound?(req: Request, res: Response, next: NextFunction, routeName: RouteName): void;
    handleError?(req: Request, res: Response, next: NextFunction, routeName: RouteName, error: Error): void;
    canList?(req: Request): Promise<boolean> | boolean;
    canDetail?(req: Request, modelData: T): Promise<boolean> | boolean;
    canInsert?(req: Request): Promise<boolean> | boolean;
    canUpdate?(req: Request): Promise<boolean> | boolean;
    canDelete?(req: Request): Promise<boolean> | boolean;
}
