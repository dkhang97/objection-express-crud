import { Request, Router } from 'express';
import { Model, RelationMappings } from 'objection';

import CrudOptions, { ExtraRoutes, ResultWrapExtras, RouteName } from './crud-options';
import getAll from './routes/get-all';
import createRouteHandler from './utils/create-route-handler';
import { combineExtraRoutes } from './utils/extra-routes';
import { hasOptions, hasRoute } from './utils/general';

export { CrudOptions, ResultWrapExtras, RouteName, ExtraRoutes };

async function dbFindById(req: Request, model: typeof Model, opts: CrudOptions<any>) {
    const query = model.query().findById(req.params.id);

    if (hasOptions(opts) && typeof opts.detailFn === 'function') {
        const detailQ = opts.detailFn(query, req);
        if (detailQ instanceof Promise) {
            await detailQ;
        }
    }

    return await query;
}

export default function objectionCrud<T extends typeof Model>(model: T, opts?: CrudOptions<T['prototype']>): Router {
    const router = Router();

    if (hasRoute(opts, 'list')) {
        router.get('/', ...combineExtraRoutes(opts, 'list', getAll(model, opts)));
    }

    if (hasRoute(opts, 'detail')) {
        router.get('/:id', ...combineExtraRoutes(opts, 'detail',
            createRouteHandler(model, opts, 'canDetail', 'detail',
                (req, res, m, data) => ({ data }), (req, m) => dbFindById(req, m, opts),
            ),
        ));
    }

    if (hasRoute(opts, 'insert')) {
        router.post('/', ...combineExtraRoutes(opts, 'insert',
            createRouteHandler(model, opts, 'canInsert', 'insert',
                async (req, res, m: T) => {
                    let relMap: RelationMappings = m.relationMappings as any;

                    if (typeof relMap === 'function') {
                        try {
                            relMap = (relMap as any)();
                        } catch {
                            // Nothing to do here
                        }
                    }

                    if (relMap && Object.keys(relMap).length) {
                        return { data: await m.query().insertGraphAndFetch(req.body, opts.insertGraphOptions) };
                    }

                    return { data: await m.query().insertAndFetch(req.body) };
                },
            ),
        ));
    }

    if (hasRoute(opts, 'updatePut')) {
        router.put('/:id', ...combineExtraRoutes(opts, 'updatePut',
            createRouteHandler(model, opts, 'canUpdate', 'updatePut',
                async (req, res, m) => ({
                    data: await m.query()
                        .updateAndFetchById(req.params.id, req.body),
                }),
            ),
        ));
    }

    if (hasRoute(opts, 'updatePatch')) {
        router.patch('/:id', ...combineExtraRoutes(opts, 'updatePatch',
            createRouteHandler(model, opts, 'canUpdate', 'updatePatch',
                async (req, res, m, item) => ({
                    data: await m.query()
                        .patchAndFetchById(req.params.id, req.body),
                }),
            ),
        ));
    }

    if (hasRoute(opts, 'delete')) {
        router.delete('/:id', ...combineExtraRoutes(opts, 'delete',
            createRouteHandler(model, opts, 'canDelete', 'delete',
                async (req, res, m) => ({
                    data: await m.query().deleteById(req.params.id),
                }),
            ),
        ));

        // router.delete('/', ...combineExtraRoutes(opts, 'delete',
        //     createRouteHandler(model, opts, 'canDelete', 'delete',
        //         async (req, res, m) => ({
        //             data: await m.query().deleteById(req.body),
        //         }),
        //     ),
        // ));
    }

    return router;
}
