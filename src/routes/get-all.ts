import { Request, RequestHandler } from 'express';
import { AnyQueryBuilder, GraphOptions, Model, QueryBuilder, raw, RelationExpression } from 'objection';
import shortid from 'shortid';

import CrudOptions, { CrudCreateQuery, CrudCreateQueryOpts, CrudQueryBuilder } from '../crud-options';
import createRouteHandler from '../utils/create-route-handler';

function initQuery<T extends typeof Model>(model: T,
    opts: CrudOptions<T['prototype']>, req: Request, { graphJoined, mainQuery }: CrudCreateQueryOpts,
    graphUsedCallback: () => void): CrudQueryBuilder<T['prototype']> {
    const assignQuery = <Q extends AnyQueryBuilder>(q: Q) => {
        const withCrudGraph = (expr: RelationExpression<any>, options?: GraphOptions) => {
            const graphQ = (graphJoined ? q.withGraphJoined : q.withGraphFetched).call(q, expr, options)
            graphUsedCallback();
            return graphQ;
        }

        return Object.assign(q, {
            withCrudGraph, isCrudGraphJoined: graphJoined, isCrudMainQuery: mainQuery,
            asCrudGraphFetched: () => initQuery(model, opts, req, { graphJoined: false }, graphUsedCallback),
            asCrudGraphJoined: () => initQuery(model, opts, req, { graphJoined: true }, graphUsedCallback),
        });
    }

    let query = assignQuery(model.query());

    if (typeof opts?.listFn === 'function') {

        const listQ = opts.listFn(query, req);
        if (listQ instanceof QueryBuilder) {
            query = assignQuery(listQ);
        }
    }

    return query;
}

class GetAllGenerator<T extends typeof Model>{
    readonly actualPage: number;
    readonly actualSize: number;
    readonly offset: number;

    constructor(
        readonly req: Request,
        readonly model: T,
        readonly opts: CrudOptions<T['prototype']>
    ) {
        const { page, pageSize } = req.query;
        let { offset } = req.query;
        let actualSize: number;
        let actualPage: number;

        if (pageSize === void 0 || pageSize === '' || (!isNaN(pageSize) && (pageSize + '').toLowerCase() !== 'all')) {
            actualSize = !isNaN(pageSize) && Math.max(+pageSize, 1) || Math.max(0, +opts?.defaultPageSize || 0) || 20;

            if (isNaN(offset)) {
                actualPage = !isNaN(pageSize) && Math.max(+page, 1) || 1;
                offset = ((actualPage - 1) * actualSize)
            }
        }

        if (isNaN(offset) && isNaN(actualPage)) {
            actualPage = 1;
        }

        this.offset = offset;
        this.actualPage = actualPage;
        this.actualSize = actualSize;
    }

    createListRangeQuery() {
        let fn = this.opts?.listRangeFn;

        if (fn === true) {
            fn = this.model.idColumn;
        }

        if ((typeof fn === 'string' && fn) || (Array.isArray(fn) && fn.length)) {
            const mainTable = '__crud_main_' + shortid.generate();
            const filterTable = '__crud_filter_' + shortid.generate();
            const idCol = fn;

            fn = ({ createQuery, queryType }, limit, offset) => {
                if (!isNaN(limit) || !isNaN(offset)) {
                    const q = createQuery().as('__crud_' + shortid.generate());
                    if (!isNaN(offset)) {
                        q.offset(offset);
                    }

                    if (queryType === 'hasMore') {
                        return q.limit(1);
                    }

                    if (!isNaN(limit)) {
                        q.limit(limit);
                    }

                    const rawInnerSQL = Model.query().from(q).distinct(idCol).toKnexQuery().toQuery();
                    return createQuery({ mainQuery: true, graphJoined: true }).alias(mainTable).clearWhere()
                        .innerJoin(raw(`(${rawInnerSQL}) as ??`, filterTable), `${filterTable}.${idCol}`, `${mainTable}.${idCol}`);
                }
            }
        }

        if (typeof fn === 'function') {
            return fn;
        }
    }

    async createHandler() {
        let graphUsed = false;
        const opts = this.opts;

        const createQuery: CrudCreateQuery<T['prototype']> = (crudOpts = {}) =>
            initQuery(this.model, opts, this.req, crudOpts, () => graphUsed = true);

        let query: AnyQueryBuilder;
        const rangeFn = this.createListRangeQuery();
        if (typeof rangeFn === 'function') {
            query = rangeFn({ createQuery, queryType: 'main' }, this.actualSize, this.offset) as any;
        }

        if (!(query instanceof QueryBuilder)) {
            query = createQuery({ graphJoined: true, mainQuery: true });
            if (!isNaN(this.actualSize)) {
                query.limit(this.actualSize);
            }
            if (!isNaN(this.offset)) {
                query.offset(this.offset);
            }
        }

        const data = await query;
        return {
            data, extra: new GetAllExtra(this, data, () => graphUsed, crudOpts => createQuery(crudOpts)),
        };
    }
}

async function wrapCountQuery(q: AnyQueryBuilder) {
    const colName = '__CNT_' + shortid.generate();
    const r = await Model.query().from(q.as('__crud_' + shortid.generate())).count('* as ' + colName);
    return +r[0][colName];
}

class GetAllExtra<T extends typeof Model>  {
    private hasMoreCache: Promise<boolean> = (typeof this.actualSize !== 'number' || this.data.length < this.actualSize)
        && Promise.resolve(false);
    private countCache: Promise<number> = typeof this.actualSize !== 'number' && Promise.resolve(this.data.length)
        || (0 < this.data.length && this.data.length < this.actualSize && Promise.resolve((this.offset || 0) + this.data.length));

    get actualSize() { return this.generator.actualSize; }
    get actualPage() { return this.generator.actualPage; }
    get offset() { return this.generator.offset; }
    get query() { return this.createQuery({ graphJoined: true }); }

    constructor(
        private generator: GetAllGenerator<T>,
        private data: any[],
        private isGraphUsed: () => boolean,
        private createQuery: CrudCreateQuery<T['prototype']>,
    ) { }

    async hasMore() {
        if (this.hasMoreCache) {
            return await this.hasMoreCache.catch(() => { this.hasMoreCache = void 0 });
        }

        const furtherOffset = this.actualSize + (this.offset || 0);

        const rangeFn = this.generator.createListRangeQuery();
        if (typeof rangeFn === 'function') {
            const q = rangeFn({ createQuery: crudOpts => this.createQuery(crudOpts), queryType: 'hasMore' }, 1, furtherOffset);
            if (q instanceof QueryBuilder) {
                return await (this.hasMoreCache = q.then(r => !!r[0]));
            }
        }

        return await (this.hasMoreCache = this.createQuery({ graphJoined: true })
            .offset(furtherOffset).limit(1).then(r => !!r[0]));
    }

    private createBaseCountQuery() {
        return this.createQuery({ graphJoined: true }).clearOrder();
    }

    private createCountFromOpts() {
        const graphUsed = this.isGraphUsed();
        const { opts, req, model } = this.generator;

        if (graphUsed || opts?.countFn) {
            if (typeof opts?.countFn === 'function') {
                const countRs = opts.countFn({
                    createQuery: crudOpts => this.createQuery(crudOpts),
                    createCountQuery: () => this.createBaseCountQuery(),
                    wrapCall: q => wrapCountQuery(q),
                }, req);
                return Promise.resolve(countRs);
            } else {
                const distinctCol = opts?.countFn || (graphUsed && model.idColumn);

                if (distinctCol) {
                    return wrapCountQuery(Model.query()
                        .from(this.createBaseCountQuery().as('__crud_inner_' + shortid.generate()))
                        .distinct(distinctCol));
                }
            }
        }
    }

    async getCount() {
        if (this.countCache) {
            return await this.countCache.catch(() => { this.countCache = void 0 });
        }

        const countFromOpts = this.createCountFromOpts();

        if (countFromOpts instanceof Promise) {
            return await (this.countCache = countFromOpts);
        }

        return await (this.countCache = wrapCountQuery(this.createBaseCountQuery()));
    }

    async getTotalPage() {
        if (isNaN(this.actualSize)) {
            return 1;
        }

        const totalItem = await this.getCount();

        if (!totalItem) {
            return 1;
        }

        return Math.ceil(totalItem / this.actualSize);
    }
}

export default function getAll<T extends typeof Model>(model: T, opts?: CrudOptions<T['prototype']>): RequestHandler {
    return createRouteHandler(model, opts, 'canList', 'list', req => new GetAllGenerator(req, model, opts).createHandler());
}
