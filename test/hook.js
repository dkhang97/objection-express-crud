/*jshint esversion: 9 */

var cfg = JSON.parse(require('fs').readFileSync('./tsconfig.json'));

require('ts-node').register({
    transpileOnly: true,
    typeCheck: false,
    files: false,
    compilerOptions: {
        ...cfg.compilerOptions,
        module: 'commonjs',
        sourceMap: false,
        declaration: false,
    }
});
