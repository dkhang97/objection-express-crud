import axios from 'axios';
import bodyParser from 'body-parser';
import { expect } from 'chai';
import express from 'express';
import { existsSync, mkdirSync, unlinkSync } from 'fs';
import { Server } from 'http';
import Knex from 'knex';
import { Model } from 'objection';
import { buildTable, Column, Table } from 'objection-annotations';

import objectionCrud from '../src';

export const EXPRESS_PORT = 54927;

@Table('todo_list')
class Todo extends Model {
    @Column('increments')
    id: number;

    @Column('string', { required: true, schema: { maxLength: 20 } })
    content: string;

    @Column('integer', { required: true })
    priority: number;
}

let server: Server;
let knex: Knex;
const app = express();

const PRE_ITEMS: Partial<Todo>[] = [
    { content: 'Buy 3 eggs', priority: 1 },
    { content: 'Buy 1 juice bottle', priority: 1 },
    { content: 'Visit DMV', priority: 2 },
    { content: 'Visit Dentist', priority: 2 },
    { content: 'Find girlfriend(S)', priority: -1 },
];

const DATA_FILENAME = './data/test.db';

// tslint:disable: no-console
describe('Test server', () => {
    describe('Init Server', () => {
        it('Create Database', async () => {
            if (!existsSync('data')) {
                mkdirSync('data');
            }

            const filename = DATA_FILENAME;

            await buildTable(knex = Knex({
                client: 'sqlite3',
                connection: { filename },
                useNullAsDefault: true,
            }), Todo);
        });

        it('Seeding Database', async () => {
            await Todo.query().delete();
            await Promise.all(PRE_ITEMS.map(async item =>
                await Todo.query().insert(item)));
        });

        it(`Start server on 127.0.0.1:${EXPRESS_PORT}`, () => {
            app.use(bodyParser.json());
            app.use('/todo', objectionCrud(Todo, {
                // routes: ['list', 'detail'],
                async resultWrap(result, req, extra) {
                    if (extra.routeName === 'list') {
                        const [totalItems, total, hasMore] = await Promise
                            .all([extra.getCount(), extra.getTotalPage(), extra.hasMore()]);

                        return {
                            items: result, paging: {
                                current: extra.actualPage,
                                totalItems, total, hasMore
                            },
                        };
                    }

                    return result;
                },
                handleError(req, res, next, routeName, err) {
                    res.sendStatus(500);
                },
            }));
            server = app.listen(EXPRESS_PORT, '127.0.0.1');
        });
    });

    describe('Consume API', () => {
        const api = axios.create({
            baseURL: `http://127.0.0.1:${EXPRESS_PORT}/todo`,
            timeout: 1000,
        });
        let items: Todo[];

        function randomItem() {
            return items[Math.floor(Math.random() * items.length)];
        }

        it('GET /', async () => {
            const { data: dataOrg } = await api.get('/');
            items = dataOrg.items;
            expect(dataOrg.paging.totalItems).to.equals(PRE_ITEMS.length);

            const pageSize = 2;
            const { data } = await api.get('/', { params: { pageSize, page: 2 } });
            expect(data.items.length).to.most(pageSize);
            expect(data.paging.totalItems).to.most(items.length);
            expect(data.paging.hasMore).to.equals(data.paging.current < data.paging.total);
        });

        it('GET /:id', async () => {
            const item = randomItem();
            const { data } = await api.get('/' + item.id);
            expect(data).to.deep.equals(item);
        });

        it('POST /', async () => {
            const content = 'Fly to New York';
            const { data } = await api.post('/', { content, priority: 7 });
            items.push(data);
            expect(data.content).to.equals(content);
        });

        it('PUT /:id', async () => {
            const item = randomItem();
            const content = 'Fly to Seattle, WA';
            const { data } = await api.put('/' + item.id, { content });
            item.content = content;
            expect(data).to.deep.equals(item);
        });

        it('PATCH /:id', async () => {
            const item = randomItem();
            const priority = 5;
            const { data } = await api.patch('/' + item.id, { priority });
            item.priority = priority;
            expect(data).to.deep.equals(item);
        });

        it('DELETE /:id', async () => {
            const { data } = await api.delete('/' + randomItem().id);
            expect(data).to.equals(1);
        });
    });

    describe('Close Server', () => {
        it('Stopping server', done => server.close(done));
        it('Close Database', async () => await knex.destroy());
    });
}).afterAll(async () => {
    console.log('All test done!');
    if (server) {
        console.log('Shutting down Express...');
        server.close();
    }

    await knex.destroy();

    try {
        unlinkSync(DATA_FILENAME);
    } catch {
        console.log('WARN: Failed to remove test database');
    }
});
