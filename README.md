# objection-express-crud

[![version](https://img.shields.io/npm/v/objection-express-crud.svg?style=flat)](https://www.npmjs.com/package/objection-express-crud) [![npm](https://img.shields.io/npm/l/objection-express-crud.svg)](https://opensource.org/licenses/MIT)

## Installation

```bash
npm i --save objection-express-crud
```

## Example

```ts
import bodyParser from 'body-parser';
import express from 'express';
import Knex from 'knex';

import { Model } from 'objection';
import { buildTable, Column, Table } from 'objection-annotations';
import objectionCrud from 'objection-express-crud';

@Table('todo_list')
class Todo extends Model {
    @Column('increments')
    id: number;

    @Column('string', { required: true, schema: { maxLength: 20 } })
    content: string;

    @Column('integer', { required: true })
    priority: number;
}

(async () => {
    await buildTable(Knex({
        client: 'sqlite3',
        connection: {
            filename: process.env['DB_FILE'],
        },
        useNullAsDefault: true,
    }), Todo);

    const app = express();
    app.use(bodyParser.json());

    // Register CRUD route
    app.use('/todo', objectionCrud(Todo, {
        async resultWrap(result, req, { routeName, getTotalPage, actualPage, getCount }) {
            if (routeName === 'list') {
                return {
                    items: result, paging: {
                        current: actualPage,
                        totalItem: await getCount(),
                        total: await getTotalPage(),
                    },
                };
            }

            return result;
        },
    }));

    app.listen(8080);
})();
```

## Options

>- `insertGraphOptions`
>- `routes:` Register express routing for objection, default option is **true**
>- `preRoutes:` Register handler(s) before objection handler
>- `postRoutes:` Register handler(s) after objection handler
>- `listFn:` Function to modify query builder for listing route
>- `detailFn:` Function to modify query builder for detail route
>- `resultWrap:` Wrap function of query result
>- `handleResponse`
>- `handleForbidden`
>- `handleNotFound`
>- `handleError`
>- `canList`
>- `canDetail`
>- `canInsert`
>- `canUpdate`
>- `canDelete`
